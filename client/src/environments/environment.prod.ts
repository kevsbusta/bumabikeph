export const environment = {
  production: true,
  token_auth_config: {
    apiBase: 'http://localhost:3000',
    oAuthBase: 'http://localhost:3000',
    oAuthCallbackPath: 'oauth_callback',
    oAuthWindowType: 'sameWindow'
  }
};
