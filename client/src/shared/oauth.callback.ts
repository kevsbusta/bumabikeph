import { Component } from '@angular/core';
import { Angular2TokenService } from "angular2-token";

@Component({
  selector: 'app-root',
  template: ''
})
export class OauthCallbackComponent {

  constructor(private tokenService: Angular2TokenService) {}

  async ngOnInit() {
    await this.tokenService.validateToken()
      .subscribe(
        res => {
          this.tokenService.processOAuthCallback();
          location.href = "/profile";
        },
        err => {
          console.error(err);
        }
      );
  }
}
