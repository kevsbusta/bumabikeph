export class Wishlist {
  id: number;
  name: string;
  description: string;
  stock: number;
  price: number;
  discount: number;
  original_price: number;
}
