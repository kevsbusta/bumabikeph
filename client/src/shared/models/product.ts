export class Product {
  id: number;
  name: string;
  description: string;
  price: number;
  wishlist: boolean;
  stock: number;
  discount: number;
  original_price: number;
  highlights: Highlight[];
  notes: Note[];
  specifications: Specification[];
  pictures: Picture[];
}

export class Highlight {
  id: number;
  value: string;
}

export class Note {
  id: number;
  value: string;
}

export class Specification {
  id: number;
  property: string;
  value: string;
}

export class Picture {
  path: string;
  url: string;
}
