import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { User } from '../models/user';

import { Angular2TokenService } from "angular2-token";

@Injectable()
export class UserService {
  constructor(private service: Angular2TokenService) { }

  getUsers() : Observable<User[]> {
    return this.service.get(`users`)
      .map((res:Response) => <User> res.json())
      .catch(this.handleError);
  }

  getUser(id: number) : Observable<User> {
    return this.service.get(`users/${id}`)
      .map((res:Response) => <User> res.json())
      .catch(this.handleError);
  }

  updateUser (data: User): Observable<User> {
    return this.service.put(`users/${data.id}`, JSON.stringify(data))
      .map((res:Response) => res.json())
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error());
  }
}

