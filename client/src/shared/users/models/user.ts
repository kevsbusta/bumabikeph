export class User {
  id: string;
  name: string;
  email: string;
  gender: string;
  birthday: Date;
  newsletter: boolean;
}
