import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { Product, Picture } from '../models/product';

import { Angular2TokenService } from "angular2-token";

@Injectable()
export class ProductService {
  constructor(private service: Angular2TokenService) { }

  getProducts(id: number = null) : Observable<Product[]> {
    return this.service.get(`products?category_id=${id}`)
      .map((res:Response) => <Product> res.json())
      .catch(this.handleError);
  }

  getProduct(id: number) : Observable<Product> {
  return this.service.get(`products/${id}`)
    .map((res:Response) => res.json())
    .catch(this.handleError);
  }

  updateProduct(product: Product) : Observable<Response> {
    return this.service.put(`products/${product.id}`, JSON.stringify(product))
      .map((res:Response) => res.json())
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error());
  }
}

