import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { Wishlist } from '../models/wishlist';

import { Angular2TokenService } from "angular2-token";

@Injectable()
export class WishlistService {
  constructor(private service: Angular2TokenService) { }

  getWishlists() : Observable<Wishlist[]> {
    return this.service.get(`wishlists`)
      .map((res:Response) => <Wishlist> res.json())
      .catch(this.handleError);
  }

  toggleWishlist(product_id: number) : Observable<Response> {
    return this.service.put(`wishlists/${product_id}`, null)
      .map((res:Response) => res.json())
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error());
  }
}

