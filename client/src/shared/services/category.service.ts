import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { Category } from '../models/category';

import { Angular2TokenService } from "angular2-token";

@Injectable()
export class CategoryService {
  constructor(private service: Angular2TokenService) { }

  getCategories() : Observable<Category[]> {
    return this.service.get(`categories`)
      .map((res:Response) => <Category> res.json())
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error());
  }
}

