import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { Category } from '../models/category';

import { Angular2TokenService } from "angular2-token";

var Dropbox = require('dropbox');

@Injectable()
export class DropboxService {
  private dbx: any;
  constructor(private service: Angular2TokenService) {

    this.dbx = new Dropbox({ accessToken: 'Nom5ueLZMS8AAAAAAAACrWaqGMXSmVUEZiBVW6gTzVVMvLFIOU7ATboEffMOh4Rs' });
  }

  uploadFile(productId, file) : Promise<any> {

    return this.dbx.filesUpload({path: `/products/${productId}/${file.name}`, contents: file})
      .then(() => {
        return this.dbx.sharingCreateSharedLink({path: `/products/${productId}/${file.name}`, short_url: false, pending_upload: { '.tag': 'file' } })
      });
  }

  getCategories() : Observable<Category[]> {
    return this.service.get(`categories`)
      .map((res:Response) => <Category> res.json())
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error());
  }
}

