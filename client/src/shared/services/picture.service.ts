import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Angular2TokenService } from "angular2-token";

@Injectable()
export class PictureService {
  constructor(private service: Angular2TokenService) { }

  deletePicture(id: number) : Observable<Response> {
    return this.service.delete(`pictures/${id}`)
      .map((res:Response) => res.json())
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error());
  }
}

