import { Component } from '@angular/core';
import { Category } from '../shared/models/category'
import { Product } from '../shared/models/product'
import { CategoryService } from '../shared/services/category.service';
import { ProductService } from '../shared/services/product.service';
import { WishlistService } from '../shared/services/wishlist.service';

@Component({
  selector: 'app-root',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent {

  public categories: Category[];
  public products: Product[];

  constructor(private categoryService: CategoryService, private productService: ProductService, private wishlistService: WishlistService) { }

  async ngOnInit() {
    await this.categoryService.getCategories()
      .subscribe(
        res => {
          this.categories = res;
        },
        err => {
          console.error(err);
        }
      )

    await this.getProducts();
  }

  private async toggleWishlist(product: Product) {
    await this.wishlistService.toggleWishlist(product.id)
      .subscribe(
        res => {
          product.wishlist = !product.wishlist;
        },
        err => {
          console.error(err);
        }
      )
  }

  private async getProducts(id: number = null) {
    await this.productService.getProducts(id)
      .subscribe(
        res => {
          this.products = res;
        },
        err => {
          console.error(err);
        }
      )
  }
}
