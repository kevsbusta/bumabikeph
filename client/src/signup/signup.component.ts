import { Component } from '@angular/core';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { Angular2TokenService, RegisterData } from "angular2-token";
import { UserService } from '../shared/users/services/user.service';
import { User } from '../shared/users/models/user';

@Component({
  selector: 'app-root',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent {
  private myDatePickerOptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  public myDatePicker: IMyDateModel;

  error = {
    isError: false,
    errorMessage: "Sample Error message."
  }

  signUpUser: RegisterData = {
    email: '',
    name: '',
    password: '',
    passwordConfirmation: '',
    gender: '',
    birthday: '',
    newsletter: ''
  };

  constructor(private tokenService: Angular2TokenService, private userService: UserService){
  }

  signUp(): void {

    this.signUpUser.birthday = this.myDatePicker.jsdate.toString();

    this.tokenService.registerAccount(this.signUpUser)
      .subscribe(
        (res) => {
          if (res.status == 200) {
            let user = res.json().data;
            user.name = this.signUpUser.name;
            user.gender = this.signUpUser.gender;
            user.birthday = this.signUpUser.birthday;
            user.newsletter = this.signUpUser.newsletter;
            
            this.userService.updateUser(<User>user)
              .subscribe(
                res => {
                  location.href = "/profile";
                },
                err => {
                  console.error(`updateUser: ${err}`);
                }
              );
          } else {
            console.error(`registerAccount ${res.status}: ${res}`);
          }
        },
        (err) => {
          console.error(`registerAccount: ${err.json()}`);
        }
      )
  }
}
