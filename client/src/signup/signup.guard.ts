import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Angular2TokenService } from "angular2-token";

@Injectable()
export class SignupRouteGuard implements CanActivate {

  constructor(private tokenService: Angular2TokenService) {}

  canActivate() {
    return !this.tokenService.userSignedIn();
  }
}
