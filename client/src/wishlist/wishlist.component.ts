import { Component } from '@angular/core';
import { Wishlist } from '../shared/models/wishlist'
import  { WishlistService } from '../shared/services/wishlist.service';

@Component({
  selector: 'app-root',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css'],
})
export class WishlistComponent {

  private wishlists: Wishlist[];
  constructor(private wishlistService: WishlistService) {
    this.wishlistService.getWishlists()
      .subscribe(
        res => {
          this.wishlists = res;
        },
        err => {
          console.error(err);
        }
      );
  }

  private async removeToWishlist(wishlist: Wishlist) {

    await this.wishlistService.toggleWishlist(wishlist.id)
      .subscribe(
        res => {
          this.wishlists = this.wishlists.filter(item => item.id != wishlist.id);
        },
        err => {
          console.error(err);
        }
      )
  }
}
