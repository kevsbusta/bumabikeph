import { environment } from "../environments/environment";

import { Component } from '@angular/core';

import { Angular2TokenService } from "angular2-token";

@Component({
  selector: 'app-root',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent {
  title = 'app';
}
