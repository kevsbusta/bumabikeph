import { Component } from '@angular/core';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { UserService } from '../../shared/users/services/user.service';
import { User } from '../../shared/users/models/user';
import { Angular2TokenService } from "angular2-token";


@Component({
  selector: 'app-profile',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalInformationComponent {
  private myDatePickerOptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  public myDatePicker: IMyDateModel;

  error = {
    isError: false,
    errorMessage: "Sample Error message."
  }

  public user: User;

  constructor(private userService: UserService, private tokenService: Angular2TokenService){
  }

  async ngOnInit() {
    await this.tokenService.validateToken()
      .subscribe(
        res => {
          let id = this.tokenService.currentUserData.id;
          this.userService.getUser(id)
            .subscribe(
              res => {
                this.user = res;
              },
              err => {
                console.error(err);
              }
            );
        },
        err => {
          console.error(err);
        }
      );
  }
}
