import { Component } from '@angular/core';
import { ProductService } from '../../shared/services/product.service'
import { Product } from '../../shared/models/product'

@Component({
  selector: 'app-admin',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class AdminProductComponent {
  private products: Product[];

  constructor(private productService: ProductService) { }

  async ngOnInit() {
    await this.productService.getProducts()
      .subscribe(
        res => {
          this.products = res;
        },
        err => {
          console.error(err);
        }
      )
  }
}
