import { Component } from '@angular/core';
import { ProductService } from '../../shared/services/product.service'
import { DropboxService } from '../../shared/services/dropbox.service'
import { PictureService } from '../../shared/services/picture.service'
import { Product } from '../../shared/models/product'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './product-overview.component.html',
  styleUrls: ['./product-overview.component.css']
})
export class AdminProductOverviewComponent {
  private product: Product;
  private id: number;

  constructor(private route: ActivatedRoute,
              private productService: ProductService,
              private dropboxService: DropboxService,
              private pictureService: PictureService) { }

  async ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
    });

    await this.productService.getProduct(this.id)
      .subscribe(
        res => {
          this.product = res;
        },
        err => {
          console.error(err);
        }
      )
  }

  onRemoved(event) {
    var pictureId = this.product.pictures.find(p => p.url === event.src)["id"];
    this.pictureService.deletePicture(pictureId)
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.error(err);
        }
      )
  }

  onUploadFinished(event) {
    this.dropboxService.uploadFile(this.id, event.file)
      .then((res) => {
        this.product.pictures.push({path: res.path, url: `${res.url}&raw=1`});
        this.productService.updateProduct(this.product);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  onUploadStateChanged(event) {
    console.log(event);
  }

  private saveProduct() {
    this.productService.updateProduct(this.product)
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.error(err);
        }
      );
  }

  private addEmptyHighlight() {
    this.product.highlights.push({id: this.product.highlights.length + 1, value: ''});
  }

  private addEmptyNote() {
    this.product.notes.push({id: this.product.notes.length + 1, value: ''});
  }

  private addEmptySpec() {
    this.product.specifications.push({id: this.product.specifications.length + 1, property: '', value: ''});
  }
}
