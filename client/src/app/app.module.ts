import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ImageUploadModule } from "angular2-image-upload";

// Components
import { AppComponent } from './app.component';
import { MainComponent } from '../main/main.component';
import { LoginComponent } from '../login/login.component';
import { SignupComponent } from '../signup/signup.component';
import { ProfileComponent } from '../profile/profile.component';
import { PersonalInformationComponent } from '../profile/personal_information/personal.component';
import { AccountDashboardComponent } from '../profile/account_dashboard/dashboard.component';
import { OauthCallbackComponent } from '../shared/oauth.callback';
import { ProductComponent } from '../product/product.component';
import { WishlistComponent } from '../wishlist/wishlist.component';
import { AdminComponent } from '../admin/admin.component';
import { AdminDashboardComponent } from '../admin/admin_dashboard/dashboard.component';
import { AdminProductComponent } from '../admin/admin_product/product.component';
import { AdminProductOverviewComponent } from '../admin/product_overview/product-overview.component';
import { ProductOverviewComponent } from '../product_overview/product-overview.component';

// Guards
import { LoginRouteGuard } from '../login/login.guard';
import { SignupRouteGuard } from '../signup/signup.guard';
import { ProfileGuard } from '../profile/profile.guard';

// Services
import { Angular2TokenService } from 'angular2-token';
import { UserService } from '../shared/users/services/user.service';
import { CategoryService } from '../shared/services/category.service';
import { ProductService } from '../shared/services/product.service';
import { WishlistService } from '../shared/services/wishlist.service';
import { DropboxService } from '../shared/services/dropbox.service';
import { PictureService } from '../shared/services/picture.service';

import { MyDatePickerModule } from 'mydatepicker';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [LoginRouteGuard] },
  { path: 'signup', component: SignupComponent, canActivate: [SignupRouteGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [ProfileGuard],
    children: [
      { path: '', component: AccountDashboardComponent },
      { path: 'personal-info', component: PersonalInformationComponent },
      { path: 'wishlist', component: WishlistComponent },
    ]
  },
  { path: 'admin', component: AdminComponent, canActivate: [],
    children: [
      { path: '', component: AdminDashboardComponent },
      { path: 'product-list', component: AdminProductComponent },
    ]
  },
  { path: 'admin/product-details/:id', component: AdminProductOverviewComponent },
  { path: 'product', component: ProductComponent },
  { path: 'product/:id', component: ProductOverviewComponent },

  { path: 'oauth_callback', component: OauthCallbackComponent },

  { path: '**', component: MainComponent },
];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    MainComponent,
    ProfileComponent,
    PersonalInformationComponent,
    AccountDashboardComponent,
    OauthCallbackComponent,
    ProductComponent,
    WishlistComponent,
    AdminComponent,
    AdminDashboardComponent,
    AdminProductComponent,
    AdminProductOverviewComponent,
    ProductOverviewComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
      // { enableTracing: true }
    ),
    MyDatePickerModule,
    ImageUploadModule.forRoot()
  ],
  providers: [
    Angular2TokenService,
    UserService,
    CategoryService,
    ProductService,
    WishlistService,
    DropboxService,
    PictureService,

    LoginRouteGuard,
    SignupRouteGuard,
    ProfileGuard,
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
