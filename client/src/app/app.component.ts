import { environment } from "../environments/environment";
import { Component } from '@angular/core';
import { Angular2TokenService } from "angular2-token";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent{
  title = 'app';
  userSignedIn: boolean;
  name: string;

  constructor(private tokenService: Angular2TokenService) {}

  async ngOnInit() {
    this.tokenService.init(environment.token_auth_config);
    this.userSignedIn = this.tokenService.userSignedIn();
    await this.getName();
  }
  
  signOut(): void {
    this.tokenService.signOut();
    location.href = "/";
  }

  private async getName() {
    if(this.userSignedIn) {
      await this.tokenService.validateToken()
        .subscribe(
          res => {
            this.name = this.tokenService.currentUserData.name;
          },
          err => {
            console.error(err);
          }
        );
    }
  }
}
