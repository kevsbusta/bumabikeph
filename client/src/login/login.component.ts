import { Component } from '@angular/core';
import { Angular2TokenService } from "angular2-token";

@Component({
  selector: 'app-root',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  title = 'app';
  error = {
    isError: false,
    errorMessage: "Sample Error message."
  };

  constructor(private tokenService: Angular2TokenService){
  }

  signIn(): void {
    this.tokenService.signIn({email: "bumabikeph@gmail.com", password: "bananas123"}).subscribe(
      res => {
        console.log('auth response:', res);
        console.log('auth response headers: ', res.headers.toJSON());
        console.log('auth response body:', res.json());
        location.href = "/profile";
      },
      err => {
        console.error('auth error:', err);
      }
    )
  }

  facebookSignIn(): void {
    this.signInOAuth('facebook');
  }

  googleSignIn(): void {
    this.signInOAuth('google_oauth2');
  }

  private signInOAuth(oAuthType: string): void {
    this.tokenService.signInOAuth(oAuthType)
      .subscribe(
        res => {
          console.log("signInOAuth: " + res);
        },
        error => {
          console.error("signInOAuth: " + error);
        }
      );
  }
}
