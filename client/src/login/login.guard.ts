import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Angular2TokenService } from "angular2-token";

@Injectable()
export class LoginRouteGuard implements CanActivate {

  constructor(private authToken: Angular2TokenService) {}

  canActivate() {
    return !this.authToken.userSignedIn();
  }
}
