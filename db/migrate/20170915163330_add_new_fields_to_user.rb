class AddNewFieldsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :gender, :string, :limit => 1
    add_column :users, :birthday, :date, default: nil
    add_column :users, :newsletter, :boolean, default: nil
  end
end
