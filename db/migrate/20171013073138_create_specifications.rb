class CreateSpecifications < ActiveRecord::Migration[5.0]
  def change
    create_table :specifications do |t|

      t.string :key
      t.string :property
      t.integer :product_id
      t.timestamps
    end

    remove_column :products, :specifications
  end
end
