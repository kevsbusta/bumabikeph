class CreateNotes < ActiveRecord::Migration[5.0]
  def change
    create_table :notes do |t|

      t.string :item
      t.integer :product_id
      t.timestamps
    end

    remove_column :products, :notes
  end
end
