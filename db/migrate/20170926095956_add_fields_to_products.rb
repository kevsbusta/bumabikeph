class AddFieldsToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :discount, :integer, :limit => 2
    add_column :products, :stock, :integer
    add_column :products, :highlights, :string
    add_column :products, :notes, :string
    add_column :products, :specifications, :string
    add_column :products, :pictures, :string
  end
end
