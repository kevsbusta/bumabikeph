class CreatePictures < ActiveRecord::Migration[5.0]
  def change
    create_table :pictures do |t|

      t.string :path
      t.string :url
      t.integer :product_id
      t.timestamps
    end

    remove_column :products, :pictures
  end
end
