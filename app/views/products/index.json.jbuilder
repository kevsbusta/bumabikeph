json.array! @products do |product|
  json.extract! product, :id, :name, :description, :price, :discount, :stock, :pictures
  json.wishlist product.in?(@wishlist_products)
  if product.discount
    json.price product.price
    discountet_amount = product.price * (product.discount.to_f / 100)
    json.discounted_price product.price - discountet_amount
  end
end