json.extract! @product, :id, :name, :description, :price, :discount, :stock, :notes, :highlights, :specifications, :pictures
if @product.discount
  json.price @product.price
  discounted_amount = @product.price * (@product.discount.to_f / 100)
  json.discounted_price @product.price - discounted_amount
end
