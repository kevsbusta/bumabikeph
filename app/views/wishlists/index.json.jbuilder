json.array! @wishlists do |wishlist|
  json.extract! wishlist, :id, :name, :description, :price, :discount, :stock
  if wishlist.discount
    json.original_price wishlist.price
    discountet_amount = wishlist.price * (wishlist.discount.to_f / 100)
    json.price wishlist.price - discountet_amount
  end
end