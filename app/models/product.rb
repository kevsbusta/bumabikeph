class Product < ApplicationRecord
  has_many :categories_products
  has_many :categories, :through => :categories_products
  has_many :pictures
  has_many :highlights
  has_many :notes
  has_many :specifications
end
