class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         # :confirmable,
         :omniauthable,

         :omniauth_providers => [:facebook, :google_oauth2]

  include DeviseTokenAuth::Concerns::User

  has_many :wishlists
  has_many :wishlist_products, :through => :wishlists, :source => :product
end