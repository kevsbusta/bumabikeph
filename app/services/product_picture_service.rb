class ProductPictureService
  def initialize(params)
    @product = params[:product]
    @params = params[:params]
    @product_params = params[:product_params]
  end

  def update_product
    @product.update(@product_params)
    save_link_picture_to_product
    save_highlights
    save_notes
    save_specifications
  end

  private

  def save_highlights
    @product.highlights.destroy_all
    @params[:highlights].each do |highlight|
      highlight.permit!
      @product.highlights << Highlight.find_or_create_by(item: highlight[:item])
    end
  end

  def save_notes
    @product.notes.destroy_all
    @params[:notes].each do |note|
      note.permit!
      @product.notes << Note.find_or_create_by(item: note[:item])
    end
  end

  def save_specifications
    @product.specifications.destroy_all
    @params[:specifications].each do |specification|
      specification.permit!
      @product.specifications << Specification.find_or_create_by(key: specification[:key], property: specification[:property])
    end
  end

  def save_link_picture_to_product
    @params[:pictures].each do |picture|
      picture.permit!
      @product.pictures << Picture.find_or_create_by(path: picture[:path], url: picture[:url])
    end
  end
end