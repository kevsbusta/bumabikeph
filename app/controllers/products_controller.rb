class ProductsController < ApplicationController
  before_action :authenticate_user!

  def index
    @wishlist_products = current_user.wishlist_products
    if category
      @products = category.products
    else
      @products = Product.all
    end

    filter_products
  end

  def show
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    update_product
  end

  private

  def filter_products
    @products = query.present? ? @products.where("lower(name) LIKE ?", "%#{query.downcase}%") : @products
  end

  def query
    params[:query]
  end

  def category_id
    params[:category_id]
  end

  def category
    Category.find_by(id: category_id)
  end

  def product_params
    params.require(:product).permit(:name, :description, :price, :stock, :discount, :highlights, :notes, :specifications, :pictures)
  end

  def update_product
    ProductPictureService.new({product: @product, product_params: product_params, params: params}).update_product
  end
end
