class PicturesController < ApplicationController

  def destroy
    picture = Picture.find(params[:id])
    picture.destroy

    render json: picture
  end
end
