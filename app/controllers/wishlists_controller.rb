class WishlistsController < ApplicationController
  before_action :authenticate_user!

  def index
    @wishlists = current_user.wishlist_products
  end

  def update
    wishlist = current_user.wishlists.find_by(product_id: params[:id])
    if wishlist
      wishlist.delete
    else
      current_user.wishlists.create(product_id: params[:id])
    end
  end

end
